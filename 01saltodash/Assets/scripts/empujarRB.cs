using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class empujarRB : MonoBehaviour
{

    public float poderEmpuje = 2f;

    private float MasaObjeto;

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //rb con el que colisionamos
        Rigidbody rb = hit.collider.attachedRigidbody;

        if (rb==null || rb.isKinematic)
        {
            return;
        }

        //si caemos encima del objeto que no se mueva
        if (hit.moveDirection.y < -0.3)
        {
            return;
        }

        MasaObjeto = rb.mass;

        Vector3 pushDireccion = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);
        rb.velocity = pushDireccion * poderEmpuje/MasaObjeto;
    }
}
