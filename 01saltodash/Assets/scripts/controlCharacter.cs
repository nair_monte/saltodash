using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlCharacter : MonoBehaviour
{

    private CharacterController player;

    private float horizontalMove;
    private float verticalMove;

    public float rapidezDesplazamiento;


    private Vector3 playerInput;

    public Camera MainCamera;
    private Vector3 camForward;
    private Vector3 camRight;
    private Vector3 movimientoJugador;


    public float gravedad = 9.8f;
    public float velocidadCaida;

    private int cantSaltos;
    public float fuerzaSalto;

    public float velocidadDash;
    private int cantDashes;


    public bool isOnRampa=false;
    private Vector3 hitNormal;
    public float velocidadDeslizar;
    void Start()
    {
        player = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        WASDyGravedad();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        hitNormal = hit.normal;
       
    }




    void WASDyGravedad()
    {
        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");

        //guardar las variables en 1 vector
        playerInput = new Vector3(horizontalMove, 0, verticalMove);

        //que no pase de cierta velocidad
        playerInput = Vector3.ClampMagnitude(playerInput, 1);

        //direcciones de la cammara
        camDirection();

        //direccion del jugador respecto a la camara
        movimientoJugador = playerInput.x * camRight + playerInput.z * camForward;

        //se aisla la velocidad del jugador
        movimientoJugador = movimientoJugador * rapidezDesplazamiento;


        //que mire hacia donde se esta moviendo
        player.transform.LookAt(player.transform.position + movimientoJugador);

        //setearGravedad
        setearGravedad();

        //habilidades dle jugador como saltar o el dash
        habilidadesJugador();

        //que se mueva
        player.Move(movimientoJugador * Time.deltaTime);

    }

    void camDirection()
    {
        camForward = MainCamera.transform.forward;
        camRight = MainCamera.transform.right;

        camForward.y = 0;
        camRight.y = 0;

        //entre 0 y 1
        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }

    void setearGravedad()
    {
        movimientoJugador.y = -gravedad * Time.deltaTime;

        //si el pj esta tocando el piso la gravedad va a ser constante
        if (player.isGrounded)
        {
            velocidadCaida = -gravedad * Time.deltaTime;
           
            //aislar la y en velocidadCaida, ya no se resetea a 0 en el update
            movimientoJugador.y = velocidadCaida;
        }
        //si no esta en el piso va a tener una aceleracion en su caida
        else
        {
            velocidadCaida -= gravedad * Time.deltaTime;

            //aislar la y en velocidadCaida, ya no se resetea a 0 en el update
            movimientoJugador.y = velocidadCaida;
        }


        RampaAbajo();
    }


    void habilidadesJugador()
    {
        if (Input.GetButtonDown("Jump") &&  (player.isGrounded || cantSaltos <1))
        {
             
                velocidadCaida = fuerzaSalto;
                movimientoJugador.y = velocidadCaida;
                cantSaltos++;
     
        }

        if (player.isGrounded)
        {
            cantSaltos = 0;
            cantDashes = 0;
        }

        if (Input.GetKeyDown(KeyCode.W) && Input.GetKeyDown(KeyCode.LeftShift) && cantDashes<2 )
        {
            //player.transform.position += new Vector3(0,0,velocidadDash);

            //player.Move(camForward * velocidadDash);

            movimientoJugador.z += 5;

           
            cantDashes++;
        }
        if (Input.GetKeyDown(KeyCode.S) && Input.GetKeyDown(KeyCode.LeftShift) && cantDashes < 2 )
        {
            //player.transform.position += new Vector3(0,0,velocidadDash);

            //player.Move(camForward * velocidadDash);

            movimientoJugador.z += -5;


            cantDashes++;
        }

        if ( Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.LeftShift) && cantDashes < 2)
        {
            //player.transform.position += new Vector3(0,0,velocidadDash);

            //player.Move(camForward * velocidadDash);

            player.transform.position += Vector3.forward;


            cantDashes++;
        }

        if ( Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.LeftShift) && cantDashes < 2 )
        {
            player.transform.position +=  Vector3.forward;

           
            

          


            cantDashes++;
        }
    }

    void RampaAbajo()
    {
        //inOnRampa es verdadero siempre que el angulo que se da entre nosotros y
        //donde estamos parados es mayor o igual al slope limit que le pusimos al character controller

        isOnRampa = Vector3.Angle(Vector3.up,hitNormal) >= player.slopeLimit;

        if (isOnRampa)
        {
            movimientoJugador.x += ((1f-hitNormal.y)*hitNormal.x) * velocidadDeslizar;
            movimientoJugador.z += hitNormal.z * velocidadDeslizar;

            //que baje tocando la rampa todo el rato 
            movimientoJugador.y += -10;
        }
    }
}
