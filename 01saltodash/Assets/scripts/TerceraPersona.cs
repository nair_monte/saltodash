using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerceraPersona : MonoBehaviour
{
    public Vector3 distanciaCamara;
    public Transform objetivo;


    [Range(0,1)] public float lerpValor;

    public float sens;
    void Start()
    {
        objetivo = GameObject.Find("Jugador").transform;
    }

    
    void Update()
    {
        
    }
    private void LateUpdate()
    {
       //que la camara se mueva desde el jugador para atras suavemente con lerp
        transform.position = Vector3.Lerp(transform.position, objetivo.position + distanciaCamara, lerpValor);

        //AngleAxis girar determinados grados de acuerdo a un eje
        //los grados los da el mouse en x y se gira alrededor del vector 3 que apunta hacia arriba
        distanciaCamara = Quaternion.AngleAxis (Input.GetAxis   ("Mouse X") *   sens, Vector3.up   )    *   distanciaCamara;
        
        transform.LookAt(objetivo);
    }
}
